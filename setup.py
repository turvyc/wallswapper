# setup.py
#
# Setup script for wallswapper-cli. For installation instructions, 
# take a gander at the README.
# 
# (c) 2013 T. Colin Strong
# ---
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from distutils.core import setup

setup(name = 'wallswapper-cli',
      version = '1.0',
      description = 'Creates a desktop slideshow.',
      author = 'T. Colin Strong',
      author_email = 'colin \\at\\ colinstrong \\dot\\ cat',
      url = 'https://bitbucket.org/turvyc/wallswapper',
      classifiers = [
          'Environment :: Console',
          'Intended Audience :: End Users/Desktop',
          'Licence :: GPL3',
          'Operating System :: Linux :: Gnome'
      ],
      packages=['wallswapper'],
     )
