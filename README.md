# wallswapper

Creates a desktop slideshow by cycling the wallpaper through a directory of images.

## Requirements

This script was developed on an Arch Linux system using the following dependencies:

- python >= 3.3.1
- gnome-shell >= 3.6.3.1

I suspect it will work just fine with any python >= 3, and perhaps for gnome-shell >= 3 as well.

UPDATE: I've tested it using Cinnamon 1.8.8, and it works fine. The program essentially hinges on
the `/usr/bin/gsettings` command, so any recent Gnome-based DE should hopefully work.

## Features

- Randomly displays every wallpaper in the queue before recycling.
- Supports recursion: load wallpapers from the subdirectories of your wallpaper folder.
- Detects and incorporates changes to the directory tree without disrupting the queue.
- Adjustable time interval between swaps.

## Usage

    wallswapper [-h] [-i N] [-r] [-v] [--version] wallpaper_dir
    
    positional arguments:
      wallpaper_dir       your wallpaper directory
    
    optional arguments:
      -h, --help          show this help message and exit
      -i N, --interval N  interval between swaps in seconds, default 180
      -r, --recurse       recurse into subdirectories of wallpaper_dir
      -v, --verbose       read about the program's inner workings
      --version           show program's version number and exit

It is currently recommended to fork the process in your shell, e.g. follow the entire command with an ampersand:
    
    wallswapper-cli -r ~/img/wallpapers &
