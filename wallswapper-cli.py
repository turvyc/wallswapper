# wallswapper-cli.py
# 
# The command line interface for wallswapper.
# 
# (c) 2013 T. Colin Strong
# ---
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import wallswapper
from time import sleep

program_name = 'wallswapper'
version = '0.1'
default_interval = 180 # 3 minutes

parser = argparse.ArgumentParser(prog = program_name)

parser.add_argument('wallpaper_dir', help = 'your wallpaper directory')
parser.add_argument('-i', '--interval', help = 'interval between swaps \
                    in seconds, default %d.' %  default_interval, metavar = 'N', 
                    type = int, default = default_interval)
parser.add_argument('-r', '--recurse', help = 'recurse into subdirectories of wallpaper_dir',
                    action = 'store_true')
parser.add_argument('-v', '--verbose', help = 'read about the program\'s inner workings', 
                    action = 'store_true')
parser.add_argument('--version', action = 'version', version = '%(prog)s ' + version)

args = parser.parse_args()

swapper = wallswapper.WallSwapper(args.wallpaper_dir)

if args.verbose:
    swapper.setVerbose()

if args.recurse:
    swapper.setRecursive()

swapper.generateQueue()

# Every programmer's nightmare, the infinite loop! This program should ideally
# be forked (i.e. following the command with '&'), or should be written as a
# daemon with a config file.
while True:
    swapper.nextWallpaper()
    sleep(args.interval)
